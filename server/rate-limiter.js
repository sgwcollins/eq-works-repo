const MemoryStore = require("./memory-store")
let localStore = {};


//const in a seperate file

module.exports = (req,res,next) =>{


    let options = {
        'key':req.ip+':'+req.originalUrl,
        'max':10,
        'windowMs':60 * 1000
    }


    if(Object.keys(localStore).length === 0 ){
        localStore =  new MemoryStore(options);
    }

    localStore.increment(options.key, function (err, currentCounter, resetTime, remaining) {

        if (err) {
            return next(err);
        }

        if (res.headersSent === false) {
            //give the amount of calls
            res.setHeader("X-RateLimit-Limit", options.max);
            res.setHeader("X-RateLimit-Remaining", remaining);
            if (resetTime instanceof Date) {
                res.setHeader("Date", new Date().toUTCString());
                res.setHeader(
                    "X-RateLimit-Reset",
                    Math.round(resetTime.getTime() / 1000)
                );
            }
        }

        if (currentCounter > options.max) {
            if (res.headersSent === false) {
                res.setHeader("Retry-After", Math.round(options.windowMs / 1000));
                return res.status(429).send("Too many requests, please try again later.");
            }

        }

        next();

    });

}



