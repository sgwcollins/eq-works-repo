const express = require('express')
const pg = require('pg')

const app = express()
// configs come from standard PostgreSQL env vars
// https://www.postgresql.org/docs/9.6/static/libpq-envars.html
const pool = new pg.Pool()

const rateCheck = require('./rate-limiter')
const cors = require('cors');

/*

Allow for local version of client to communicate with the server

 */
app.use(cors());

app.use(rateCheck);
const queryHandler = (req, res, next) => {
  pool.query(req.sqlQuery).then((r) => {
    return res.json(r.rows || [])
  }).catch(next)
}


app.get('/', (req, res) => {
  res.send('Welcome to EQ Works 😎')
})


app.get('/events/hourly', (req, res, next) => {
  req.sqlQuery = `
    SELECT date, hour, events
    FROM public.hourly_events
    ORDER BY date, hour
    LIMIT 168;
  `
  return next()
}, queryHandler)

app.get('/events/daily', (req, res, next) => {
  req.sqlQuery = `
    SELECT date, SUM(events) AS events
    FROM public.hourly_events
    GROUP BY date
    ORDER BY date
    LIMIT 7;  
  `
  return next()
}, queryHandler)


app.get('/stats/hourly', (req, res, next) => {
  req.sqlQuery = `
    SELECT p.name,date, hour, impressions, clicks, revenue
  FROM public.hourly_stats as hs
           INNER JOIN poi as p ON hs.poi_id = p.poi_id
  ORDER BY date, hour
    LIMIT 15;  
  `
  return next()
}, queryHandler)

app.get('/stats/daterange', (req, res, next) => {

  let start = req.query.startDate;
  let end = req.query.endDate;
  let metric = req.query.metric

  req.sqlQuery = `
   SELECT p.name,
       p.lon,
       p.lat,
       SUM(${metric}) AS ${metric}
    FROM public.hourly_stats as hs
    INNER JOIN poi as p ON hs.poi_id = p.poi_id
    WHERE hs.date >= '${start}' AND hs.date <= '${end}'
    GROUP BY p.poi_id;`



  return next()
}, queryHandler)

app.get('/stats/daily', (req, res, next) => {
  req.sqlQuery = `
    SELECT date,
        SUM(impressions) AS impressions,
        SUM(clicks) AS clicks,
        SUM(revenue) AS revenue
    FROM public.hourly_stats
    GROUP BY date
    ORDER BY date
    LIMIT 7;
  `
  return next()
}, queryHandler)

app.get('/poi', (req, res, next) => {
  req.sqlQuery = `
    SELECT *
    FROM public.poi;
  `
  return next()
}, queryHandler)



app.listen(process.env.PORT || 5555, (err) => {
  if (err) {
    console.error(err)
    process.exit(1)
  } else {
    console.log(`Running on ${process.env.PORT || 5555}`)
  }
})

// last resorts
process.on('uncaughtException', (err) => {
  console.log(`Caught exception: ${err}`)
  process.exit(1)
})
process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason)
  process.exit(1)
})
