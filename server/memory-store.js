"use strict";

function calculateNextResetTime(windowMs) {
    const d = new Date();
    d.setMilliseconds(d.getMilliseconds() + windowMs);
    return d;
}

function MemoryStore(options){
    let windowMs = options.windowMs
    let hits = {}
    let reset = calculateNextResetTime(windowMs);
    let max = options.max;

    this.increment = function(key, incrementCallback) {
        if (hits[key]) {
            hits[key]++;
        } else {
            hits[key] = 1;
        }

        incrementCallback(null, hits[key], reset ,Math.max(max - hits[key], 0));
    }


    this.resetAll = function() {
        hits = {};
        reset = calculateNextResetTime(windowMs);
    };

    const interval = setInterval(this.resetAll, windowMs);
    if (interval.unref) {
        interval.unref();
    }


}

module.exports = MemoryStore;
