import React from 'react';
import ReactDOM from 'react-dom';
import Dashboard from './components/Dashboard';
import 'semantic-ui-css/semantic.min.css'
import 'daterangepicker/daterangepicker.css'


ReactDOM.render(<Dashboard />, document.getElementById('root'));
