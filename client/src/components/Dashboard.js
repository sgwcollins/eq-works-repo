import React, {Component} from 'react';
import { Header,Container ,Divider, Grid,Segment } from 'semantic-ui-react'
import Charts from './Visualize/Charts'
import Map from './Visualize/Maps'
import {API_URL} from '../constant'
import axios from 'axios';
import Table from './Table/TableContainer';

class Dashboard extends Component{


    constructor(props) {
        super(props);

        this.state = {
            'chartData': [],
            'tableData': []
        }

    }

    componentDidMount() {

        axios.get(`${API_URL}stats/daily`).then((resp) =>{
            this.setState({
                'chartData':resp.data
            })
        })

        axios.get(`${API_URL}stats/hourly`).then((resp) =>{
            this.setState({
                'tableData':resp.data
            })
        })

    }

    render(){

        const {
            chartData,
            tableData
        } = this.state;

        return (
            <div>
                <Container>
                    <Divider className={'hidden'} />
                    <Header as='h1' textAlign='center'>
                        EQ Works Dashboard
                    </Header>
                    <Segment>
                        <Grid columns={2} padded>
                            <Grid.Column>
                                <Header as='h3' textAlign='center'>
                                    Impressions for the week
                                </Header>
                                <Charts index={1} data={chartData} metric={'impressions'} type={'column'}/>
                            </Grid.Column>
                            <Grid.Column>
                                <Header as='h3' textAlign='center'>
                                    Clicks for the week
                                </Header>
                                <Charts index={2} data={chartData} metric={'clicks'} type={'line'}/>
                            </Grid.Column>
                        </Grid>
                    </Segment>
                    <Segment>
                        <Header as='h3' textAlign='center'>
                            Data Table
                        </Header>
                        <Table
                            tableHead={["Name", "Date", "Hour", "Impression","Clicks","Revenue"]}
                            tableData={tableData}
                        />
                    </Segment>
                    <Segment>
                        <Header as='h3' textAlign='center'>
                            Map
                        </Header>
                        <Map index={3} />
                    </Segment>
                </Container>
            </div>
        );
  }

}

export default Dashboard;
