
import {Table} from 'semantic-ui-react'
import React from "react";
import {hourConverter} from "../../util"


const TableBody = (tableData) =>{

    return (
        <Table.Body>
            {tableData['data'].map((prop,key) => {

                return (
                    <Table.Row key={key}>{Object.keys(prop).map((column) =>{

                        let value = prop[column];

                        if(column === 'revenue'){
                            value = '$'+parseFloat(value).toFixed(2);
                        }else if(column === 'date'){
                            let date = new Date(value)
                            value = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();

                        }else if(column === 'hour'){
                            value = hourConverter(value.toString());
                        }


                        return (
                            <Table.Cell key={key +'-'+column}><React.Fragment>{value}</React.Fragment></Table.Cell>
                        )

                    })}</Table.Row>
                )


            })}
        </Table.Body>
    )


}


export default TableBody
