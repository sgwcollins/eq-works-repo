import React, {Component} from 'react';
import {Table} from 'semantic-ui-react'
import THead from './THead'
import TBody from './TBody'
import { Icon, Input } from 'semantic-ui-react'
import  fuzzysort  from 'fuzzysort'
import parse from 'html-react-parser';



class TableContainer extends Component{

        constructor(props){
            super(props);

            this.state = {
                searchText : '',
                tableData : []
            }
        }

        componentDidUpdate(prevProps, prevState,snapshot) {

            const {
                tableData,
            } = this.props;


            if(JSON.stringify(prevProps.tableData)!==JSON.stringify(tableData)){

                this.setState({ tableData: tableData });

            }
        }

        updateTableData = (event) =>{
            let {tableData} = this.props;
            let searchText = event.target.value
            let fuzzyData =  fuzzysort.go(searchText, tableData,{key:'name'})
            let searchTableData = []

            if(searchText !== '') {
                fuzzyData.forEach((data) => {
                    let searchObject = Object.assign({}, data.obj)
                    searchObject['name'] = parse(fuzzysort.highlight(data, '<mark>', '</mark>'))
                    searchTableData.push(searchObject)
                });

                this.setState({
                    tableData: searchTableData
                })
            }else {

                this.setState({
                    tableData
                })

            }
        }


        render() {
            const {tableHead} = this.props
            const {tableData} = this.state

            return(
                <React.Fragment>
                    <Input icon placeholder='Search...'>
                        <input onChange={this.updateTableData} />
                        <Icon name='search' />
                    </Input>
                    <Table celled>
                        <THead columns={tableHead} />
                        <TBody data={tableData} />
                    </Table>
                </React.Fragment>
            )
        }

}

export default TableContainer
