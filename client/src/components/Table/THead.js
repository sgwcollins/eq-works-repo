
import {Table} from 'semantic-ui-react'
import React from 'react';

const TableHead = (tableHead) =>{

    return (
        <Table.Header>
            <Table.Row>
                {tableHead['columns'].map((prop, key) => {
                    return (
                        <Table.HeaderCell
                            key={key}
                        >
                            {prop}
                        </Table.HeaderCell>
                    );
                })}
            </Table.Row>
        </Table.Header>
    )


}


export default TableHead
