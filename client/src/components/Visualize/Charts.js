import React, {Component} from "react";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import {capitalize} from "../../util";
import {Loader,Dimmer} from 'semantic-ui-react'

class Charts extends Component{

    componentDidUpdate(prevProps, prevState, snapshot) {

        const {
            data,
            index,
            metric,
            type
        } = this.props;


        if(JSON.stringify(prevProps.data)!==JSON.stringify(data)){


            let chart = am4core.create(`chart-${index}`, am4charts.XYChart);

            chart.paddingRight = 20;

            let graphData = [];
            data.forEach((entry)=>{
                graphData.push({ date: new Date(entry.date), value: entry[metric] });
            })
            chart.data = graphData

            let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
            dateAxis.renderer.grid.template.location = 0;

            let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.tooltip.disabled = false;
            valueAxis.renderer.minWidth = 35;

            let series = {}

            if(type === 'column')
                series = chart.series.push(new am4charts.ColumnSeries());
            else
                series = chart.series.push(new am4charts.LineSeries());

            series.dataFields.dateX = "date";
            series.dataFields.valueY = "value";

            series.tooltipText = `${capitalize(metric)}: {valueY.value}`;
            chart.cursor = new am4charts.XYCursor();

            this.chart = chart;

        }




    }

    componentWillUnmount() {
        if (this.chart) {
            this.chart.dispose();
        }
    }

    render(){

        const {index} = this.props;

        return (
            <div id={`chart-${index}`} style={{ width: "100%", height: "500px" }}>
                <Dimmer active inverted>
                    <Loader size='massive'>Loading</Loader>
                </Dimmer>
            </div>
        );
    }

}

export default Charts;
