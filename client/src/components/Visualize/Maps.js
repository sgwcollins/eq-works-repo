import React, {Component} from "react";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldLow";
import daterangepicker from 'daterangepicker'
import {Loader, Dimmer, Select, Button, Grid, Input, Icon} from 'semantic-ui-react'
import moment from "moment";
import $ from "jquery";
import axios from "axios";
import {capitalize} from "../../util";
import {API_URL} from "../../constant";


class Maps extends Component{


    constructor(props){
        super(props)

        this.state = {
            'metric':'',
            'startDate':moment('01/01/2015').format(),
            'endDate':moment().format(),
            'mapData':{}
        }
    }


    componentDidMount() {

        const {
            index,
        } = this.props;

        $('input[name="daterange"]').daterangepicker({
            timePicker: true,
            startDate: moment('01/01/2015'),
            endDate: moment(),
        }, (start, end)  => {
            this.setState({
                startDate:moment(start).format('YYYY-MM-DD'),
                end:moment(end).format('YYYY-MM-DD'),
            })
        });

        let map = am4core.create(`chart-${index}`, am4maps.MapChart);

        map.paddingRight = 20;

        map.geodata = am4geodata_worldLow;

        map.projection = new am4maps.projections.Miller();

        map.zoomControl = new am4maps.ZoomControl();

        map.zoomControl.slider.height = 100;

        var polygonSeries = map.series.push(new am4maps.MapPolygonSeries());

        polygonSeries.useGeodata = true;

        var polygonTemplate = polygonSeries.mapPolygons.template;
        polygonTemplate.tooltipText = "{name}";
        polygonTemplate.fill = am4core.color("#74B266");


        var hs = polygonTemplate.states.create("hover");
        hs.properties.fill = am4core.color("#367B25");


        polygonSeries.exclude = ["AQ"];


        polygonTemplate.propertyFields.fill = "fill";

        this.map = map;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        const {
            mapData,
            metric
        } = this.state;



        if(JSON.stringify(prevState.mapData)!==JSON.stringify(mapData)){
            let imageSeries = this.map.series.push(new am4maps.MapImageSeries());

            let imageSeriesTemplate = imageSeries.mapImages.template;
            let circle = imageSeriesTemplate.createChild(am4core.Circle);
            circle.radius = 4;
            circle.fill = am4core.color("#B27799");
            circle.stroke = am4core.color("#FFFFFF");
            circle.strokeWidth = 2;
            circle.nonScaling = true;
            circle.tooltipText = "{title}";

            imageSeriesTemplate.propertyFields.latitude = "latitude";
            imageSeriesTemplate.propertyFields.longitude = "longitude";

            mapData.forEach((data)=>{
                imageSeries.data.push({
                    'latitude':data.lat,
                    'longitude':data.lon,
                    'title':`${data.name}\n ${capitalize(metric)}: ${data[metric]}`
                })
            })
        }

    }


    setMetric = (e,{ value }) =>{
        this.setState({
            'metric':value
        })
    }

    getMapData = () =>{
        const {metric,startDate,endDate} = this.state;

        if(metric === ''){
            alert('select a metric')
            return false
        }


        axios.get(`${API_URL}stats/daterange`,{
            params:{
                metric:metric,
                startDate:moment(startDate).format('YYYY-MM-DD'),
                endDate:moment(endDate).format('YYYY-MM-DD'),
            }
        }).then((resp) =>{
            this.setState({
                'mapData':resp.data
            })
        }).catch((resp)=>{
            console.log(resp);
        })


    }


    componentWillUnmount() {
        if (this.map) {
            this.map.dispose()
        }
    }

    render(){

        const {index} = this.props;

        const metrics = [
            { key: 'impressions', value: 'impressions', text: 'Impression' },
            { key: 'clicks', value: 'clicks', text: 'Clicks' },
            { key: 'revenue', value: 'revenue', text: 'Revenue' },
        ];


        return (
            <React.Fragment>

                <Grid textAlign='center'  columns={3}>
                    <Grid.Column>
                        <Select style={{float:'left'}} placeholder='Select your metric' options={metrics} onChange={this.setMetric} />
                    </Grid.Column>
                    <Grid.Column>
                        <Input icon placeholder='Search...'>
                            <input type="text" name="daterange" />
                            <Icon name='calendar' />
                        </Input>

                    </Grid.Column>
                    <Grid.Column>
                        <Button style={{float:'right'}} primary onClick={this.getMapData}>Submit</Button>
                    </Grid.Column>
                </Grid>


                <div id={`chart-${index}`} style={{ width: "100%", height: "500px" }}>
                    <Dimmer active inverted>
                        <Loader size='massive'>Loading</Loader>
                    </Dimmer>
                </div>
            </React.Fragment>
        );
    }

}

export default Maps;
